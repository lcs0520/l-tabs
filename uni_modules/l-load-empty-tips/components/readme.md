### l-load-empty-tips
列表加载 及空状态组件，组件名：``l-load-empty-tips``

### 使用方式
####  此组件用于列表页面（购物车列表、商品列表等等）的加载文案提示和无数据提示；
####  

在 ``template`` 中使用组件
#### 基本用法
```html
	<view>
		<ul>
			<li v-for="(item, index) in list" :key="index">{{ item }}</li>
		</ul>
		<l-load-empty-tips
			:show="list"
			:isLoading="isLoading"
			:lastLength="lastLength"
			:limit="pageLimit"
			eText="购物车没有商品哦"
			eDesc="快去选点心爱的商品吧"
			lText="我是有底线的"
		></l-load-empty-tips>
	</view>
```

```js
	export default {
	    data() {
	        return {
						list: 0,
						pageLimit: 20, 
						isLoading: false,
						lastLength: 0 
					}
						
			},
    onReachBottom() {
			// 判断每次请求回来的数据数据 和 分页的limit  大小 
        if (this.lastLength === this.pageLimit) {
            this.getList()
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            if (this.isLoading) return
            this.isLoading = true
            setTimeout(() => {
                this.isLoading = false
                this.list += this.pageLimit
			// 模拟某次请求返回的数量小于 this.pageLimit,说明数据已经到底了
                if (this.list > 50) {
                    this.lastLength = 2
                } else {
                    this.lastLength = this.pageLimit
                }
            }, 1000)
        }
    }
	}
```

![load.gif](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-2e4bdcac-2e0b-4664-a10c-7cab0b216e44/1f6ba89a-172a-487e-b749-458d71ca6e4f.gif)
	
![empty.gif](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-2e4bdcac-2e0b-4664-a10c-7cab0b216e44/41f51593-ae4d-43eb-8c18-8ca9d3d8f74a.gif)

### 属性说明

|属性名			|类型								|默认值				|是否必填	|说明														|
|---				|----								|---					|---			|---														|
|show				|Boolean&#124;Number|false				|是				|数据列表不为空 或 数据列表长度	|
|limit			|Number							|20						|是				| 分页请求的数量								|
|isLoading	|Boolean						|false				|是				| 是否正在请求中								|
|lastLength	|Number							|0						|是				| 请求回来的数据数量						|
|img				|String							|empty.png		|否				| 空状态时 显示的图片						|
|eWidth			|Number							|202					|否				| 空状态时 显示的图片宽度				|
|lText			|String							|'没有更多了'	|否				| 数据加载到底提示文案					|
|eText			|String							|'没有数据哦'	|否				| 空状态提示的文案							|
|eDesc			|String							|''						|否				| 空状态提示的文案二						|


### slot
|名字			|说明	
|---				|----		
|默认插槽				|可以更改加载中的文案， 比如换成loading的动态图标		